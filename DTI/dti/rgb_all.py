from apa102_pi.driver import apa102

strip = apa102.APA102(num_led=150, order='rgb')

strip.clear_strip()

strip.set_pixel_rgb(0, 0x00FF00)  # Green
strip.set_pixel_rgb(1, 0x00FF00)  # Green
strip.set_pixel_rgb(2, 0x00FF00)  # Green
strip.set_pixel_rgb(3, 0x00FF00)  # Green
strip.set_pixel_rgb(4, 0x00FF00)  # Green
strip.set_pixel_rgb(5, 0x00FF00)  # Green
strip.set_pixel_rgb(6, 0x00FF00)  # Green
strip.set_pixel_rgb(7, 0x00FF00)  # Green
strip.set_pixel_rgb(8, 0x00FF00)  # Green
strip.set_pixel_rgb(9, 0x00FF00)  # Green
strip.set_pixel_rgb(10, 0x00FF00)  # Green
strip.set_pixel_rgb(11, 0x00FF00)  # Green
strip.set_pixel_rgb(12, 0x00FF00)  # Green
strip.set_pixel_rgb(13, 0x00FF00)  # Green
strip.set_pixel_rgb(14, 0x00FF00)  # Green
strip.set_pixel_rgb(15, 0x00FF00)  # Green
strip.set_pixel_rgb(16, 0x00FF00)  # Green
strip.set_pixel_rgb(17, 0x00FF00)  # Green
strip.set_pixel_rgb(18, 0x00FF00)  # Green
strip.set_pixel_rgb(19, 0xFF0000)  # Red
strip.set_pixel_rgb(20, 0xFF0000)  # Red
strip.set_pixel_rgb(21, 0xFF0000)  # Red
strip.set_pixel_rgb(22, 0xFF0000)  # Red
strip.set_pixel_rgb(23, 0xFF0000)  # Red
strip.set_pixel_rgb(24, 0xFF0000)  # Red
strip.set_pixel_rgb(25, 0xFF0000)  # Red
strip.set_pixel_rgb(26, 0xFF0000)  # Red
strip.set_pixel_rgb(27, 0xFF0000)  # Red
strip.set_pixel_rgb(28, 0xFF0000)  # Red
strip.set_pixel_rgb(29, 0xFF0000)  # Red
strip.set_pixel_rgb(30, 0xFF0000)  # Red
strip.set_pixel_rgb(31, 0xFF0000)  # Red
strip.set_pixel_rgb(32, 0xFF0000)  # Red
strip.set_pixel_rgb(33, 0xFF0000)  # Red
strip.set_pixel_rgb(34, 0xFF0000)  # Red
strip.set_pixel_rgb(35, 0xFF0000)  # Red
strip.set_pixel_rgb(36, 0xFF0000)  # Red
strip.set_pixel_rgb(37, 0xFF0000)  # Red
strip.set_pixel_rgb(38, 0x0000FF)  # Blue
strip.set_pixel_rgb(39, 0x0000FF)  # Blue
strip.set_pixel_rgb(40, 0x0000FF)  # Blue
strip.set_pixel_rgb(41, 0x0000FF)  # Blue
strip.set_pixel_rgb(42, 0x0000FF)  # Blue
strip.set_pixel_rgb(43, 0x0000FF)  # Blue
strip.set_pixel_rgb(44, 0x0000FF)  # Blue
strip.set_pixel_rgb(45, 0x0000FF)  # Blue
strip.set_pixel_rgb(46, 0x0000FF)  # Blue
strip.set_pixel_rgb(47, 0x0000FF)  # Blue
strip.set_pixel_rgb(48, 0x0000FF)  # Blue
strip.set_pixel_rgb(49, 0x0000FF)  # Blue
strip.set_pixel_rgb(50, 0x0000FF)  # Blue
strip.set_pixel_rgb(51, 0x0000FF)  # Blue
strip.set_pixel_rgb(52, 0x0000FF)  # Blue
strip.set_pixel_rgb(53, 0x0000FF)  # Blue
strip.set_pixel_rgb(54, 0x0000FF)  # Blue
strip.set_pixel_rgb(55, 0x0000FF)  # Blue
strip.set_pixel_rgb(56, 0x0000FF)  # Blue
strip.set_pixel_rgb(57, 0x0000FF)  # Blue
strip.set_pixel_rgb(58, 0x0000FF)  # Blue
strip.set_pixel_rgb(59, 0x0000FF)  # Blue

strip.show()
