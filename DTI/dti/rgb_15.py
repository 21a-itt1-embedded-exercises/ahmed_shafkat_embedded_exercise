from apa102_pi.driver import apa102


strip = apa102.APA102(num_led=150, order='rgb')


strip.clear_strip()


strip.set_pixel_rgb(0, 0xFF0000)  # Red
strip.set_pixel_rgb(1, 0xFF0000)  # Red
strip.set_pixel_rgb(2, 0xFF0000)  # Red
strip.set_pixel_rgb(3, 0xFF0000)  # Red
strip.set_pixel_rgb(4, 0xFF0000)  # Red
strip.set_pixel_rgb(5, 0xFF0000)  # Red
strip.set_pixel_rgb(6, 0xFF0000)  # Red
strip.set_pixel_rgb(7, 0xFF0000)  # Red
strip.set_pixel_rgb(8, 0xFF0000)  # Red
strip.set_pixel_rgb(9, 0xFF0000)  # Red
strip.set_pixel_rgb(10, 0xFF0000)  # Red
strip.set_pixel_rgb(11, 0xFF0000)  # Red
strip.set_pixel_rgb(12, 0xFF0000)  # Red
strip.set_pixel_rgb(13, 0xFF0000)  # Red
strip.set_pixel_rgb(14, 0xFF0000)  # Red
strip.set_pixel_rgb(15, 0x00FF00)  # Green
strip.set_pixel_rgb(16, 0x00FF00)  # Green
strip.set_pixel_rgb(17, 0x00FF00)  # Green
strip.set_pixel_rgb(18, 0x00FF00)  # Green
strip.set_pixel_rgb(19, 0x00FF00)  # Green
strip.set_pixel_rgb(20, 0x00FF00)  # Green
strip.set_pixel_rgb(21, 0x00FF00)  # Green
strip.set_pixel_rgb(22, 0x00FF00)  # Green
strip.set_pixel_rgb(23, 0x00FF00)  # Green
strip.set_pixel_rgb(24, 0x00FF00)  # Green
strip.set_pixel_rgb(25, 0x00FF00)  # Green
strip.set_pixel_rgb(26, 0x00FF00)  # Green
strip.set_pixel_rgb(27, 0x00FF00)  # Green
strip.set_pixel_rgb(28, 0x00FF00)  # Green
strip.set_pixel_rgb(29, 0x00FF00)  # Green
strip.set_pixel_rgb(30, 0x0000FF)  # Blue
strip.set_pixel_rgb(31, 0x0000FF)  # Blue
strip.set_pixel_rgb(32, 0x0000FF)  # Blue
strip.set_pixel_rgb(33, 0x0000FF)  # Blue
strip.set_pixel_rgb(34, 0x0000FF)  # Blue
strip.set_pixel_rgb(35, 0x0000FF)  # Blue
strip.set_pixel_rgb(36, 0x0000FF)  # Blue
strip.set_pixel_rgb(37, 0x0000FF)  # Blue
strip.set_pixel_rgb(38, 0x0000FF)  # Blue
strip.set_pixel_rgb(39, 0x0000FF)  # Blue
strip.set_pixel_rgb(40, 0x0000FF)  # Blue
strip.set_pixel_rgb(41, 0x0000FF)  # Blue
strip.set_pixel_rgb(42, 0x0000FF)  # Blue
strip.set_pixel_rgb(43, 0x0000FF)  # Blue
strip.set_pixel_rgb(44, 0x0000FF)  # Blue
strip.set_pixel_rgb(45, 0x0000FF)  # Blue
strip.set_pixel_rgb(46, 0x0000FF)  # Blue
strip.set_pixel_rgb(47, 0x0000FF)  # Blue

strip.show()

#strip.cleanup()
