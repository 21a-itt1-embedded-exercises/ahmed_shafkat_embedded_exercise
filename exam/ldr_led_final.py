import RPi.GPIO as GPIO	#Importing Rpi.GPIO library as GPIO
import time		#Importing time from machine


GPIO.setmode(GPIO.BCM)	#Defying the GPIO from the library
delay = 0.5	#Setting delay between readings
value = 0	#Setting initial light value
ldr = 20	#Setting ldr GPIO Pin number 20
led = 21	#Setting led GPIO Pin number 21
GPIO.setup(led, GPIO.OUT)	#Setting the GPIO output for LED
GPIO.output(led, False)		#Initially turning off the LED

def rc_time (ldr):		#Creating new function with parameter ldr
    count = 0			#Initial count value
    GPIO.setup(ldr, GPIO.OUT)	#Setting LDR pin as an output
    GPIO.output(ldr, False)	#Turning off the LDR pin
    time.sleep(delay)		#wait delay in secound
    
    GPIO.setup(ldr, GPIO.IN)	#Setting the GPIO of ldr as an input
    
    while (GPIO.input(ldr) == 0): #Condition while the ldr reading = 0 
        count += 1	#Add one to the variable 'count'
        
    return count	#Returne to the counted value
    
try:
    while True:			#Infinite loop
        print("Ldr Value:")	#Printing ldr reading value
        value = rc_time(ldr)	#Save the count value from 're_time'
        print(value)		#printing value
        if ( value > 2500 ): 	#if the ldr value is more than 2500 then
            print("Lights are ON")	#Print 'Lights are ON'
            GPIO.output(led, True)	#Turning ON the LED
        if ( value <= 2500 ):	#if ldr value is equal or less than 2500
            print("Lights are OFF")	#Print 'Lights are OFF'
            GPIO.output(led, False)	#Turning OFF the LED

except KeyboardInterrupt:	#If we interrupt from the keyboard
    pass			#End the program

finally:			#After program ends
    GPIO.cleanup()		#Clean GPIO for next time


