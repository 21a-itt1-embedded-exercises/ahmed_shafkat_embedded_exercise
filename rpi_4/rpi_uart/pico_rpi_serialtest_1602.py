import serial
from time import sleep, strftime
from datetime import datetime
from PCF8574 import PCF8574_GPIO
from Adafruit_LCD1602 import Adafruit_CharLCD
from subprocess import Popen, PIPE

lcd_i2c_addr = 0x27
mcp = PCF8574_GPIO(lcd_i2c_addr)
lcd = Adafruit_CharLCD(pin_rs=0, pin_e=2, pins_db=[4,5,6,7], GPIO=mcp)

mcp.output(3,1)     # turn on LCD backlight
lcd.begin(16,2)     # set number of LCD lines and columns
lcd.message("Welcome, Ahmed!")
sleep(1)
lcd.clear()

try:
	with serial.Serial('/dev/serial0', 9600) as ser:
		while(True):
			x = ser.readline()
			x = x.decode('utf-8')
			x = x[1:-2]
			print(f'type: {type(x)}, message: {x}')
			sleep(0.5)
			tmp,pres,hum = x.split()
			tmp = tmp[1:-2]
			pres = pres[1:-2]
			hum = hum[1:-1]
			lcd.setCursor(0,0)  # set cursor position
			lcd.message(tmp +"    "+ hum + '\n' )
			lcd.message(pres)
			sleep(2)
except KeyboardInterrupt:
	print('program closed.....')
	lcd.clear()
	#lcd.setCursor(0,0)
	lcd.message('Program Closed')
	sleep(1)
	lcd.clear()
	exit()

