from machine import Pin, SoftI2C # from the machine library
import BME280 as bme280 # the bme280 library
import utime

attemp=1
i2c = SoftI2C(scl=Pin(1), sda=Pin(0), freq=10000)

bme = bme280.BME280(i2c=i2c, address=your_bme280_address_here)

print(bme.values)

while true:
    print("attemp number: {}".format(attemp))
    print("")
    attemp+=1
    
    (temperature, pressure, humidity) = bme.values
    print(temperature)
    print(pressure)
    print(humidity)
    
    print("")
    utime.sleep(5)
