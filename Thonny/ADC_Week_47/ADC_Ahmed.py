import machine
import utime

adc0 = machine.ADC(26)
adc1 = machine.ADC(27)
conversion_factor = 3.3 / (65535)

while True:
    potentiometer = adc0.read_u16()
    voltage = potentiometer * conversion_factor
    ldr = adc1.read_u16()
    print("Voltage is:", voltage, "Light is:", ldr)
    utime.sleep(0.5)